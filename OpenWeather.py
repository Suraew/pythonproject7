OpenWeatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'


class WeatherMain:
    """Основноое описание погоды"""
    def __init__(self, temp, feels_like, pressure, humidity, temp_min, temp_max, sea_level, grnd_level):
        self.temp = temp  # Температура.
        self.feels_like = feels_like  # Человеческое восприятие температуры
        self.pressure = pressure  # Атмосферное давление (на уровне моря, если нет данных sea_level или grnd_level), гПа
        self.humidity = humidity  # Влажность, %
        self.temp_min = temp_min  # Минимальная температура на данный момент.
        self.temp_max = temp_max  # Максимальная температура на данный момент.
        self.sea_level = sea_level  # Атмосферное давление на уровне моря, гПа
        self.grnd_level = grnd_level  # Атмосферное давление на уровне земли, гПа


class WeatherDescription:
    """Описание погоды"""
    def __init__(self, id, main, description, icon):
        self.id = id
        self.main = main
        self.description = description
        self.icon = icon


class Wind:
    """Характеристики ветра"""
    def __init__(self, speed, deg, gust):
        self.speed = speed  # Скорость ветра
        self.deg = deg  # Направление ветра, градусы
        self.gust = gust  # Порыв ветра


class Clouds:
    """Характеристики облачности"""
    def __init__(self, all):
        self.all = all  # Облачность, %


class Rain:
    """Характеристики дождя"""
    def __init__(self, _1h, _3h):
        self._1h = _1h  # (при наличии) Объем дождя за последний 1 час, мм.
        self._3h = _3h  # (при наличии) Объем дождя за последние 3 часа, мм.


class Snow:
    """Характеристики снега"""
    def __init__(self, _1h, _3h):
        self._1h = _1h  # (при наличии) Объем снега за последний 1 час, мм.
        self._3h = _3h  # (при наличии) Объем снега за последние 3 часа, мм.


class Sys:
    """Расположение города"""
    def __init__(self, type, id, country, sunrise, sunset):
        self.type = type  # Внутренний параметр
        self.id = id  # Внутренний параметр
        self.country = country  # Код страны (GB, JP и т. д.)
        self.sunrise = sunrise  # Время восхода солнца, unix, UTC
        self.sunset = sunset  # Время заката, unix, UTC


class Coord:
    """Координаты города"""
    def __init__(self, lon, lat):
        self.lon = lon  # Долгота местоположения
        self.lat = lat  # Широта местоположения


class CityWeather:
    """Описание погоды в городе N"""
    def __init__(self, city_lon, city_lat, \
                    weather_id, weather_main, weather_description, weather_icon, \
                 city_base, \
                    main_temp, main_feels_like, main_pressure, main_humidity, \
                    main_temp_min, main_temp_max, main_sea_level, main_grnd_level, \
                 city_visibility, \
                    wind_speed, wind_deg, wind_gust, \
                    clouds_all, \
                    rain_1h, rain_3h, \
                    snow_1h, snow_3h, \
                 city_dt, \
                    sys_type, sys_id, sys_country, sys_sunrise, sys_sunset,
                 city_timezone, \
                 city_id, \
                 city_name, \
                 city_cod
                 ):
        self.coord = Coord(lon=city_lon, lat=city_lat)
        self.weather = WeatherDescription(id=weather_id, main=weather_main, description=weather_description, \
                                          icon=weather_icon)
        self.base = city_base  # Внутренний параметр
        self.main  = WeatherMain(temp=main_temp, feels_like=main_feels_like, pressure=main_pressure, \
                                 humidity=main_humidity, temp_min=main_temp_min, temp_max=main_temp_max, \
                                 sea_level=main_sea_level, grnd_level=main_grnd_level)
        self.visibility = city_visibility  # Видимость, метр.
        self.wind = Wind(speed=wind_speed, deg=wind_deg, gust=wind_gust)
        self.clouds = Clouds(all=clouds_all)
        self.rain = Rain(_1h=rain_1h, _3h=rain_3h)
        self.snow = Snow(_1h=snow_1h, _3h=snow_3h)
        self.dt = city_dt  # Время расчета данных, unix, UTC
        self.sys = Sys(type=sys_type, id=sys_id, country=sys_country, sunrise=sys_sunrise, sunset=sys_sunset)
        self.timezone = city_timezone  # Сдвиг в секундах от UTC
        self.id = city_id  # Идентификатор города.
        self.name = city_name  # Название города.
        self.cod = city_cod  # Внутренний параметр

def CityWeatherJSON(weather_data):
    """Создает объект класса CityWeather из JSON"""
    return CityWeather(city_lon=weather_data['coord']['lon'],
        city_lat=weather_data['coord']['lat'],
            weather_id=weather_data['weather'][0]['id'],
            weather_main=weather_data['weather'][0]['main'],
            weather_description=weather_data['weather'][0]['description'],
            weather_icon=weather_data['weather'][0]['icon'],
        city_base=weather_data['base'],
            main_temp=weather_data['main']['temp'],
            main_feels_like=weather_data['main']['feels_like'],
            main_temp_min=weather_data['main']['temp_min'],
            main_temp_max=weather_data['main']['temp_max'],
            main_pressure=weather_data['main']['pressure'],
            main_humidity=weather_data['main']['humidity'],
            main_sea_level=0 if weather_data['main'].get('sea_level') == None else weather_data['main']['sea_level'],
            main_grnd_level=0 if weather_data['main'].get('grnd_level') == None else weather_data['main']['grnd_level'],
        city_visibility=weather_data['visibility'],
            wind_speed=0 if weather_data['wind'].get('speed') == None else weather_data['wind']['speed'],
            wind_deg=0 if weather_data['wind'].get('deg') == None else weather_data['wind']['deg'],
            wind_gust=0 if weather_data['wind'].get('gust') == None else weather_data['wind']['gust'],
        clouds_all=weather_data['clouds']['all'],
            rain_1h=0 if weather_data.get('rain') == None else weather_data['rain']['1h'],
            rain_3h=0 if weather_data.get('rain') == None
                      else 0 if weather_data['rain'].get('3h') == None
                             else weather_data['rain']['3h'],
            snow_1h=0 if weather_data.get('snow') == None else weather_data['snow']['1h'],
            snow_3h=0 if weather_data.get('snow') == None
                      else 0 if weather_data['snow'].get('3h') == None
                             else weather_data['snow']['3h'],
        city_dt=weather_data['dt'],
            sys_type=0 if weather_data['sys'].get('type') == None else weather_data['sys']['type'],
            sys_id=0 if weather_data['sys'].get('id') == None else weather_data['sys']['id'],
            sys_country=weather_data['sys']['country'],
            sys_sunrise=weather_data['sys']['sunrise'],
            sys_sunset=weather_data['sys']['sunset'],
        city_timezone=weather_data['timezone'],
        city_id=weather_data['id'],
        city_name=weather_data['name'],
        city_cod=weather_data['cod']
    )
