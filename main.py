import asyncio
import json
import re
import aiohttp
from OpenWeather import *
from http import HTTPStatus

async def async_get_city_weather(city_name):
    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.openweathermap.org/data/2.5/weather?q={city_name}'
                               f'&appid={OpenWeatherAPI}&units=metric') as resp:
            if resp.status == HTTPStatus.OK:
                response_body = await resp.text()
                print(response_body)
                city_weather.append(CityWeatherJSON(json.loads(response_body)))


async def get_all_city_weather():
    for city_name in city_list:
        task = async_get_city_weather(city_name)
        tasks.append(task)
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    city_list = []
    city_weather = []
    tasks = []
    with open('cities.txt', 'r', encoding='utf-8') as city_file:
        for line in city_file.readlines():
            city = re.search(r'[A-Z][^\d-]{,20}', line)
            if city != None:
                city_list.append(city.group(0)[:-1])

    asyncio.run(get_all_city_weather())

    for city in city_weather:
        print(f'Погода в городе {city.name}: температура {city.main.temp}, влажность {city.main.humidity},')
        print(f'давление {city.main.pressure}, {"ветра нет" if city.wind.speed == 0 else "ветер " + str(city.wind.speed) + " м/с"}')
    print('TheEnd. v.1.1_07_09_2023')